# Simple Demuxer
A simple demuxer, intended to be used with MKV files produced by OBS with three audio channels.

## Requirements
- `pipenv`
- `pyenv` (probably)
- `tk`
	- Example for Arch Linux: `sudo pacman -S tk`


## Getting Started
1. `pipenv install`
2. `pipenv shell`
3. `python simple-demuxer.py`


## Windows instructions
- Install Python (`tk` ships with Python these days)
- Use the `.bat` script to launch the program (or read it and run the commands manually since it assumes some things about the installation filepath)
- Run `pipenv install` from within WSL (ex: Debian) but run the `.bat` from the host.

TL;DR: Windows is a tough one because Tkinter needs a DISPLAY to draw to but WSL does not provide one (within reason/defaults/etc.) so we have to launch it from Windows to use DISPLAY.
