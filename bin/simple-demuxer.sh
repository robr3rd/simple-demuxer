#! /usr/bin/env bash

# Set project root
# Note: `export` is to make it available to any child scripts
if [ "$ROOT_PATH" == '' ]; then
	export ROOT_PATH
	ROOT_PATH="$(git rev-parse --show-toplevel)"
fi

cd "$ROOT_PATH" || exit

pipenv run python simple-demuxer.py
