#! /usr/bin/env python3
import os
from platform import uname
import subprocess
import tkinter as tk
from tkinter.filedialog import askopenfilename

def demux():
	"""Demux the file with ffmpeg"""
	source_filepath = lbl_filepath['text']
	source_filename = os.path.splitext(source_filepath)[0]
	
	if uname().system == 'Windows':  # if running from Windows
		print('Adjusting for Windows...')
		source_filepath = subprocess.run(['bash', '-c', f"wslpath '{source_filepath}'"], stdout=subprocess.PIPE).stdout.decode('utf-8').rstrip()
		source_filename = subprocess.run(['bash', '-c', f"wslpath '{source_filename}'"], stdout=subprocess.PIPE).stdout.decode('utf-8').rstrip()
	elif 'microsoft' in uname().release.lower():  # if running from WSL, convert full paths to '/mnt/*'
		print('Adjusting for Windows (WSL)...')
		source_filepath = subprocess.run(['wslpath', f"{source_filepath}"], stdout=subprocess.PIPE).stdout.decode('utf-8').rstrip()
		source_filename = subprocess.run(['wslpath', f"{source_filename}"], stdout=subprocess.PIPE).stdout.decode('utf-8').rstrip()

	probe_command = f"ffprobe -i '{source_filepath}'"  # TODO: this + "| grep 'Stream' | grep -[matchonly] '#' | awk -F: '{print $0:a:$1}'"
	demux_command = f"ffmpeg -stats -i '{source_filepath}' -c copy -map 0:a:0 '{source_filename}.combined.m4a' -c copy -map 0:a:1 '{source_filename}.desktop.m4a' -c copy -map 0:a:2 '{source_filename}.microphone.m4a'"  # for video, add "'-c copy -map 0:v '{source_filename}.video.mp4'" and read up on HWAccel options (https://trac.ffmpeg.org/wiki/HWAccelIntro)
	print(f"Demuxing '{source_filepath}' using '{source_filename}' as the base filename.")
	print(f"Running the following command: {demux_command}")
	subprocess.run(['bash', '-c', f"time {demux_command}"])

	print(f"Finished demuxing '{source_filepath}'. The demuxed files are located in the same folder as the original .mkv file.")


def select_file():
	"""Select a file to demux"""
	filepath = askopenfilename(filetypes=[('Matroska Videos', '*.mkv')])
	filename = os.path.basename(filepath)
	if not filepath:
		return
	lbl_filepath.config(text=filepath)
	window.title(f"Simple Demuxer - {filename}")


# window setup
window = tk.Tk()
window.title('Simple Demuxer')
window.resizable(width=False, height=False)


# greeting
lbl_greeting = tk.Label(master=window, text='This application extracts single-channel video (.mp4) and multi-channel audio (.m4a) from a .mkv file.')
lbl_audio_disclaimer = tk.Label(master=window, text='It assumes three (3) audio channels: combined (#0), desktop (#1), and microphone (#2).')


# BEGIN Entry Frame
frm_entry = tk.Frame(master=window)

lbl_filepath_prompt = tk.Label(master=frm_entry, text='Please select a .mkv file: ')
btn_filepicker = tk.Button(master=frm_entry, text='Choose file...', command=select_file)
lbl_filepath_label = tk.Label(master=frm_entry, text='Filepath:')
lbl_filepath = tk.Label(master=frm_entry, text='...choose a file to see the selected path...')

# render Frame
lbl_filepath_prompt.grid(row=0, column=0)
btn_filepicker.grid(row=0, column=1)
lbl_filepath_label.grid(row=1, column=0)
lbl_filepath.grid(row=1, column=1)
# END Entry Frame


# CTA button
btn_demux = tk.Button(master=window, text='Run Demuxer', command=demux)


# window render
lbl_greeting.grid(row=0)
lbl_audio_disclaimer.grid(row=1)
frm_entry.grid(row=2)
btn_demux.grid(row=3)


window.mainloop()
